import {CREATE_MESSAGE_SUCCESS, FETCH_MESSAGES_SUCCESS, MODAL_CLOSE, MODAL_SHOW} from "./action";

const initialState = {
    messages: [],
    modalShow: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case MODAL_SHOW:
            return {...state, modalShow: true};
        case MODAL_CLOSE:
            return {...state, modalShow: false};
        case CREATE_MESSAGE_SUCCESS:
            return {...state, modalShow: false};
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messages: action.messages};
        default:
            return state
    }
};

export default reducer