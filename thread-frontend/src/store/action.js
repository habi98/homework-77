import axios from '../axios-thread';
export const MODAL_SHOW = 'MODAL_SHOW';
export const modalShow = () => ({type: MODAL_SHOW});

export const modaltToShow = () => {
    return dispatch => {
        dispatch(modalShow())
    }
};


export const MODAL_CLOSE = 'MODAL_CLOSE';

export const modalClose = () => ({type: MODAL_CLOSE});

export const modalToClose = () => {
    return dispatch => {
        dispatch(modalClose())
    }
};

export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';

export const fetchMessagesSuccess = (messages) => ({type: FETCH_MESSAGES_SUCCESS, messages});

export const fetchToMessages = () => {
    return dispatch => {
        axios.get('/messages').then(response => {
            dispatch(fetchMessagesSuccess(response.data))
        })
    }
};

export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const createMessageSuccess = () => ({type: CREATE_MESSAGE_SUCCESS});

export const createToMessage = data => {
    return dispatch => {
        axios.post('/messages', data).then(response => {
            dispatch(createMessageSuccess());
            dispatch(fetchToMessages());
        })
    }
};

