import React, {Component, Fragment} from 'react';
import './App.css';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import NewMessage from "./containers/NewMessage/NewMessage";
import Container from "./components/Container/Container";
import Messages from "./containers/Messages/Messages";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Toolbar/>
            <Container>
            <NewMessage/>
             <Messages/>
            </Container>
        </Fragment>

    );
  }
}

export default App;
