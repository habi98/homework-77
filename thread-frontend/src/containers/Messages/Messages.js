import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchToMessages} from "../../store/action";
import './Messages.css'
import MessageThumbnail from "../../components/MessageThumbnail/MessageThumbnail";

class Messages extends Component {
    componentDidMount() {
        this.props.fetchToMessages()
    }

    render() {
        return (
            <Fragment>
                {this.props.messages.map(message => {
                    return (
                        <div key={message.id} className="message">

                            <p>{message.author}</p>
                            <MessageThumbnail image={message.image}/>
                            <span>{message.message}</span>
                        </div>
                    )
                })}
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    messages: state.messages
});

const mapDispatchToProps = dispatch => ({
    fetchToMessages: () => dispatch(fetchToMessages())
});

export default connect(mapStateToProps, mapDispatchToProps)(Messages);