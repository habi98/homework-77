import React, {Component} from 'react';
import AddFormMessage from "../../components/AddFormMessage/AddFormMessage";
import Modal from "../../components/UI/Modal/Modal";
import {connect} from "react-redux";
import {createToMessage, modalToClose, modaltToShow} from "../../store/action";

const styles = {
    float: 'right',
    background: 'orange',
    border: '1px solid orange',
    color: '#fff',
    padding: '5px 5px',
    fontSize: '18px'
};

class NewMessage extends Component {
    render() {
        return (
            <div>
                <h2 style={{color: 'orange'}}>
                Message
                    <button onClick={this.props.modalToShow} style={styles}>Add Message</button>
                </h2>
                <Modal show={this.props.modalShow}>
                    <AddFormMessage close={this.props.modalToClose} create={this.props.createToMessage}/>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    messages: state.message,
    modalShow: state.modalShow
});

const mapDispatchToProps = dispatch => ({
    modalToShow: () => dispatch(modaltToShow()),
    modalToClose: () => dispatch(modalToClose()),
    createToMessage: data => dispatch(createToMessage(data)),

});

export default connect(mapStateToProps, mapDispatchToProps)(NewMessage);