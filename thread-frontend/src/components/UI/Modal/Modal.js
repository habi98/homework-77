import React, {Fragment} from 'react';
import './Modal.css'


const Modal = props => {
    return (
        <Fragment>
            <div
                className="Modal"
                style={
                    {transform: props.show ? 'translateY(0)':  'translateY(-280vh)',
                    }}
             >
                {props.children}
            </div>
        </Fragment>
    );
};

export default Modal;
