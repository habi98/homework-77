import React, {Component} from 'react';
import './AddFormMessage.css'

class AddFormMessage extends Component {
    state = {
      author: '',
      message: '',
      image: ''
    };


    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });
        this.props.create(formData)
    };
    valueChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChange = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <form onSubmit={this.submitFormHandler}>
                <div className="items">
                    <label className="label">Author</label>
                    <div className="item">
                        <input className="input" type="text" value={this.state.author} onChange={this.valueChange} name="author"/>
                    </div>
                </div>
                <div className="items">
                    <label className="label">Message</label>
                    <div className="item">
                        <textarea className="input" rows="10" cols="45" value={this.state.message} onChange={this.valueChange} name="message" required/>
                    </div>
                </div>
                <div className="items">
                    <label className="label">File</label>
                    <div className="item">
                        <input className="input" id="name"  onChange={this.fileChange}  name="image" type="file" style={{border: 'none'}}/>
                    </div>
                </div>
                    <div className="item" style={{marginLeft: '22%'}}>
                        <button type="submit" className="btn-add">Add </button>
                        <button onClick={this.props.close} className="btn-close">Close</button>
                    </div>
            </form>
        );
    }
}

export default AddFormMessage;