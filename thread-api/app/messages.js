const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const fileMs = require('../fileMs');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
         cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
         cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
     res.send(fileMs.getItems())
});


router.post('/',upload.single('image'), (req, res) => {

     const message = req.body;

     if (req.file) {
          message.image = req.file.filename
     }
     if (req.body.message !== '') {
         if(!req.body.author) {
             message.author = 'Anonymous'
         }
         message.id = nanoid();
         fileMs.addItem(message);
         res.send({message: 'OK'})
     } else {
          req.status(400).send({error: "message not"})
     }
});

module.exports = router;

