const fs = require('fs');

const fileName = './ms.json';

let array = [];


module.exports ={
    init() {
        try {
            const fileContent = fs.readFileSync(fileName);
            array = JSON.parse(fileContent)
        } catch(e){
            array = []
        }

    },
    getItems() {
        return array
    },
    addItem(item) {
        array.push(item);
        this.save()
    },
    save() {
        fs.writeFileSync(fileName, JSON.stringify(array, null, 2))
    }
};