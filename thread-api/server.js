const express = require('express');
const messages = require('./app/messages');
const cors = require('cors');
const fileMs = require('./fileMs.js');


fileMs.init();
const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());
const port = 8000;

app.use('/messages', messages);


app.listen(port, () => {
    console.log(`Server started on ${port} port`)
});
